import string
import random
import hashlib
import time

example_challenge = '9fa12dGLo21aasd12312sad456'


def generation(challenge=example_challenge, size=25):
    answer = ''.join(random.choice(string.ascii_lowercase +
                                   string.ascii_uppercase +
                                   string.digits) for x in range(size))

    attempt = challenge + answer

    return attempt, answer

shaHash = hashlib.sha256()

def testAttempt():
    Found = False
    start = time.time()

    while Found == False:
        attempt, answer = generation()
        shaHash.update(attempt)
        solution = shaHash.hexdigest()
        if solution.startswith('0000'):
            timeTook = time.time() - start
            print 'time took:', timeTook
            print 'hash de:', attempt
            print solution
            Found = True

    return answer

answer = testAttempt()

print 'resposta:', answer

def check(challenge=example_challenge,answer=answer):
    attempt = challenge + answer
    print 'hash de:', attempt
    result = shaHash.hexdigest()
    print result

check()

