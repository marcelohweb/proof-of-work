import random

blockHeight = 111388

bits = '453062093'
difficulty = float('55,589.52'.replace(',',''))
block_hash = '00000000000019c6573a179d385f6b15f4b0645764c4960ee02b33a3c7117d1e'

hex_bits = hex(int(bits))
shift = '0x%s' % hex_bits[2:4]
shift_int = int(shift, 16)
value = '0x%s' % hex_bits[4:]
value_int = int('012dcd', 16)
target = value_int * 2 ** (8 * (shift_int - 3))
hex_target = hex(target)


print hex_target